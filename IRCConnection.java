package org.harrynoob.irc;

import org.harrynoob.irc.event.Event;
import org.harrynoob.irc.event.EventCreator;
import org.harrynoob.irc.event.EventListener;
import org.harrynoob.irc.event.IRCParams;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: harrynoob
 */
public final class IRCConnection implements Runnable, Closeable {

    private final List<EventListener> eventListenerList = new ArrayList<>();
    private final IRCParams params;
    private volatile boolean closed = false;
    private Socket socket;
    public IRCConnection(IRCParams params) {
        this.params = params;
    }

    public void addEventListener(EventListener el) {
        eventListenerList.add(el);
    }

    public void removeEventListener(EventListener el) {
        eventListenerList.remove(el);
    }

    public EventListener[] getEventListeners() {
        return eventListenerList.toArray(new EventListener[eventListenerList.size()]);
    }

    public IRCParams getParams() { return params; }

    @Override
    public void run() {
        try {
            socket = createSocket(params);
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            EventCreator ec = new EventCreator(this);
            String line;
            while (!closed && (line = br.readLine()) != null) {
                Event e = ec.createEvent(line);
            }
        } catch (UnknownHostException e) {
            throw new RuntimeException("Incorrect hostname specified: " + params.getServer(), e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() throws IOException {
        closed = true;
    }

    private Socket createSocket(IRCParams params) throws IOException {
        if(params.isSsl()) {
            SocketFactory ssf = SSLSocketFactory.getDefault();
            return ssf.createSocket(params.getServer(), params.getPort());
        } else {
            return new Socket(params.getServer(), params.getPort());
        }
    }
}
