package org.harrynoob.irc.event;

/**
 * Author: harrynoob
 */
public final class IRCParams {

    private final String server;
    private final String nick;
    private final String user;
    private final int port;
    private final boolean ssl;

    public IRCParams(String server, String nick, String user, int port, boolean ssl) {
        this.server = server;
        this.nick = nick;
        this.user = user;
        this.port = port;
        this.ssl = ssl;
    }

    public String getServer() {
        return server;
    }

    public String getNick() {
        return nick;
    }

    public String getUser() {
        return user;
    }

    public int getPort() {
        return port;
    }

    public boolean isSsl() {
        return ssl;
    }

}
