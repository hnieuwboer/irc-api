package org.harrynoob.irc.event;

/**
 * Author: harrynoob
 */
public final class RawEvent {

    private final String prefix;
    private final String command;
    private final String[] params;
    public RawEvent(String prefix, String command, String[] params) {
        this.prefix = prefix;
        this.command = command;
        this.params = params;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getCommand() {
        return command;
    }

    public String[] getParams() {
        return params;
    }

}
