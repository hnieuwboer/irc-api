package org.harrynoob.irc.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: harrynoob
 */
public final class EventListener {

    private final Object listenerInstance;
    private Map<Class<? extends Event>, List<Method>> eventMethodMap = new HashMap<>();

    public EventListener(Object object) {
        this.listenerInstance = object;
        for (Method m : object.getClass().getMethods()) {
            if (m.isAnnotationPresent(EventBus.class)) {
                Class<?> paramType = m.getParameterTypes()[0];
                if (Event.class.isAssignableFrom(paramType)) {
                    Class<? extends Event> eventClass = paramType.asSubclass(Event.class);
                    List<Method> methodList = eventMethodMap.get(eventClass);
                    if(methodList != null) {
                        methodList.add(m);
                    } else {
                        methodList = new ArrayList<>(1);
                        methodList.add(m);
                        eventMethodMap.put(eventClass, methodList);
                    }
                }
            }
        }
    }

    public void onEvent(Event event) {
        for (Method method : eventMethodMap.get(event.getClass())) {
            if (method != null) {
                try {
                    method.invoke(listenerInstance, event);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}