package org.harrynoob.irc.event;

import org.harrynoob.irc.IRCConnection;
import org.harrynoob.irc.event.impl.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: harrynoob
 */
public final class EventCreator {

    private final IRCConnection connection;
    public EventCreator(IRCConnection connection) {
        this.connection = connection;
    }

    public Event createEvent(String rawLine) {
        int firstSpacePos = rawLine.indexOf(' ');
        String prefix = null;
        if(rawLine.charAt(0) == ':') {
            prefix = rawLine.substring(1, firstSpacePos);
            rawLine = rawLine.substring(firstSpacePos + 1);
        }
        firstSpacePos = rawLine.indexOf(' ');
        String command = rawLine.substring(0, firstSpacePos);
        char[] paramPart = rawLine.substring(firstSpacePos + 1).toCharArray();
        List<String> params = new ArrayList<>();
        StringBuilder currParam = new StringBuilder();
        boolean delimReached = false, justSpaced = false;
        for (char c : paramPart) {
            if (c == ':' && justSpaced) {
                delimReached = true;
                justSpaced = false;
            } else if (c == ' ' && !delimReached) {
                params.add(currParam.toString());
                currParam = new StringBuilder();
                justSpaced = true;
            } else {
                currParam.append(c);
                justSpaced = false;
            }
        }
        params.add(currParam.toString());
        String[] realParams = params.toArray(new String[params.size()]);
        for (EventType et : EventType.values()) {
            if (et.name().equals(command)) {
                Class<? extends Event> eventClazz = et.getClazz(realParams);
                try {
                    Constructor<? extends Event> constructor = eventClazz.getDeclaredConstructor(
                            IRCConnection.class, RawEvent.class
                    );
                    RawEvent rawEvent = new RawEvent(prefix, command, realParams);
                    return constructor.newInstance(
                            connection, rawEvent
                    );
                } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private enum EventType {
        JOIN(JoinEvent.class),
        MODE(ModeEvent.class),
        NICK(NickEvent.class),
        NOTICE(NoticeEvent.class),
        PART(PartEvent.class),
        PING(PingEvent.class),
        PRIVMSG(PrivMsgEvent.class) {
            @Override
            public Class<? extends Event> getClazz(String[] params) {
                if(params[1].charAt(1) == '\u0001') {
                    return CtcpEvent.class;
                }
                return super.getClazz(params);
            }
        },
        TOPIC(TopicEvent.class);
        private final Class<? extends Event> clazz;
        private EventType(Class<? extends Event> clazz) {
            this.clazz = clazz;
        }

        public Class<? extends Event> getClazz(String[] params) {
            return clazz;
        }

    }

    private static final Map<String, Class<? extends Event>> EVENT_CLASS_MAP = new HashMap<String, Class<? extends Event>>() {{
        put("JOIN", JoinEvent.class);
        put("MODE", ModeEvent.class);
        put("NICK", NickEvent.class);
        put("NOTICE", NoticeEvent.class);
        put("PART", PartEvent.class);
        put("PING", PingEvent.class);
        put("PRIVMSG", PrivMsgEvent.class);
        put("TOPIC", TopicEvent.class);
    }};

}
