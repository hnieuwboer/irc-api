package org.harrynoob.irc.event;

import org.harrynoob.irc.IRCConnection;

/**
 * Author: harrynoob
 */
public abstract class Event {

    private final IRCConnection connection;
    private final RawEvent rawEvent;
    public Event(IRCConnection connection, RawEvent rawEvent) {
        this.connection = connection;
        this.rawEvent = rawEvent;
    }

    public RawEvent getRawEvent() {
        return rawEvent;
    }

    public IRCConnection getConnection() {
        return connection;
    }

}
