package org.harrynoob.irc.event.impl;

import org.harrynoob.irc.IRCConnection;
import org.harrynoob.irc.event.Event;
import org.harrynoob.irc.event.RawEvent;

/**
 * Author: harrynoob
 */
public abstract class MsgEvent extends Event {

    private final String sender, receiver, message;
    public MsgEvent(IRCConnection connection, RawEvent rawEvent) {
        super(connection, rawEvent);
        sender = rawEvent.getPrefix();
        receiver = rawEvent.getParams()[0];
        message = rawEvent.getParams()[1];
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getMessage() {
        return message;
    }

}
