package org.harrynoob.irc.event.impl;

import org.harrynoob.irc.IRCConnection;
import org.harrynoob.irc.event.Event;
import org.harrynoob.irc.event.RawEvent;

/**
 * Author: harrynoob
 */
public final class ModeEvent extends Event {

    private final String invoker;
    private final String modeChanges;
    private final String[] modeParams;
    public ModeEvent(IRCConnection connection, RawEvent rawEvent) {
        super(connection, rawEvent);
        invoker = rawEvent.getPrefix();
        modeChanges = rawEvent.getParams()[0];
        modeParams = new String[rawEvent.getParams().length - 1];
        System.arraycopy(rawEvent.getParams(), 1, modeParams, 0, modeParams.length);
    }

    public String getInvoker() {
        return invoker;
    }

    public String getModeChanges() {
        return modeChanges;
    }

    public String[] getModeParams() {
        return modeParams;
    }

}
