package org.harrynoob.irc.event.impl;

import org.harrynoob.irc.IRCConnection;
import org.harrynoob.irc.event.RawEvent;

/**
 * Author: harrynoob
 */
public final class CtcpEvent extends MsgEvent {
    private final String ctcpMessage;
    public CtcpEvent(IRCConnection connection, RawEvent rawEvent) {
        super(connection, rawEvent);
        ctcpMessage = rawEvent.getParams()[1].substring(1);
    }

    public String getCtcpMessage() {
        return ctcpMessage;
    }
}
