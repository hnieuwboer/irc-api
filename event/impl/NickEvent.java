package org.harrynoob.irc.event.impl;

import org.harrynoob.irc.IRCConnection;
import org.harrynoob.irc.event.Event;
import org.harrynoob.irc.event.RawEvent;

/**
 * Author: harrynoob
 */
public final class NickEvent extends Event {

    private final String newNick, oldNick;
    public NickEvent(IRCConnection connection, RawEvent rawEvent) {
        super(connection, rawEvent);
        oldNick = rawEvent.getPrefix();
        newNick = rawEvent.getParams()[0];
    }

    public String getNewNick() {
        return newNick;
    }

    public String getOldNick() {
        return oldNick;
    }

}
