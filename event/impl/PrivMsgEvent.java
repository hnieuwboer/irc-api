package org.harrynoob.irc.event.impl;

import org.harrynoob.irc.IRCConnection;
import org.harrynoob.irc.event.RawEvent;

/**
 * Author: harrynoob
 */
public final class PrivMsgEvent extends MsgEvent {

    public PrivMsgEvent(IRCConnection connection, RawEvent rawEvent) {
        super(connection, rawEvent);
    }

}
