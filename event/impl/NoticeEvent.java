package org.harrynoob.irc.event.impl;

import org.harrynoob.irc.IRCConnection;
import org.harrynoob.irc.event.RawEvent;

/**
 * Author: harrynoob
 */
public final class NoticeEvent extends MsgEvent {

    public NoticeEvent(IRCConnection connection, RawEvent rawEvent) {
        super(connection, rawEvent);
    }

}
