package org.harrynoob.irc.event.impl;

import org.harrynoob.irc.IRCConnection;
import org.harrynoob.irc.event.Event;
import org.harrynoob.irc.event.RawEvent;

/**
 * Author: harrynoob
 */
public final class PartEvent extends Event {

    private final String user, channel, reason;
    public PartEvent(IRCConnection connection, RawEvent rawEvent) {
        super(connection, rawEvent);
        user = rawEvent.getPrefix();
        channel = rawEvent.getParams()[0];
        reason = rawEvent.getParams().length > 1 ? rawEvent.getParams()[2] : null;
    }

    public String getUser() {
        return user;
    }

    public String getChannel() {
        return channel;
    }

    public String getReason() {
        return reason;
    }

}
