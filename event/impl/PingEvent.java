package org.harrynoob.irc.event.impl;

import org.harrynoob.irc.IRCConnection;
import org.harrynoob.irc.event.Event;
import org.harrynoob.irc.event.RawEvent;

/**
 * Author: harrynoob
 */
public final class PingEvent extends Event {

    private final String pingCode;
    public PingEvent(IRCConnection connection, RawEvent rawEvent) {
        super(connection, rawEvent);
        this.pingCode = rawEvent.getParams()[0];
    }

    public String getPingCode() {
        return pingCode;
    }

}
