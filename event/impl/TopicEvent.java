package org.harrynoob.irc.event.impl;

import org.harrynoob.irc.IRCConnection;
import org.harrynoob.irc.event.Event;
import org.harrynoob.irc.event.RawEvent;

/**
 * Author: harrynoob
 */
public final class TopicEvent extends Event {

    private final String user, channel, newTopic;
    public TopicEvent(IRCConnection connection, RawEvent rawEvent) {
        super(connection, rawEvent);
        user = rawEvent.getPrefix();
        channel = rawEvent.getParams()[0];
        newTopic = rawEvent.getParams()[1];
    }

    public String getUser() {
        return user;
    }

    public String getChannel() {
        return channel;
    }

    public String getNewTopic() {
        return newTopic;
    }

}
