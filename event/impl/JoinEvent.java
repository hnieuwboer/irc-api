package org.harrynoob.irc.event.impl;

import org.harrynoob.irc.IRCConnection;
import org.harrynoob.irc.event.Event;
import org.harrynoob.irc.event.RawEvent;

/**
 * Author: harrynoob
 */
public final class JoinEvent extends Event {

    private final String user, channel;
    public JoinEvent(IRCConnection connection, RawEvent rawEvent) {
        super(connection, rawEvent);
        this.user = rawEvent.getPrefix();
        this.channel = rawEvent.getParams()[0];
    }

    public String getUser() {
        return user;
    }

    public String getChannel() {
        return channel;
    }

}
